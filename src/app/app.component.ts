import { Component } from '@angular/core';
import { NotifierService } from './notifier.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Chat-app';

  constructor (private notify:NotifierService) { 
    notify.sendNotify("")
  }
  
}
