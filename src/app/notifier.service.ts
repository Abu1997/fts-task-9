import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotifierService {

  notify:BehaviorSubject<any>=new BehaviorSubject<any>(null);

  constructor() { }

   sendNotify(value :any){
    this.notify.next(value);
   }
}
