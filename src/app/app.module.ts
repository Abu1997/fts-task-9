import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Person1Component } from './person1/person1.component';
import { Person2Component } from './person2/person2.component';
import { Person3Component } from './person3/person3.component';
import { AllMessageComponent } from './all-message/all-message.component';
import { ImportantMessageComponent } from './important-message/important-message.component';
import { NotifierService } from './notifier.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    Person1Component,
    Person2Component,
    Person3Component,
    AllMessageComponent,
    ImportantMessageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [NotifierService],
  bootstrap: [AppComponent]
})
export class AppModule { }
