import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NotifierService } from '../notifier.service';

@Component({
  selector: 'app-person3',
  templateUrl: './person3.component.html',
  styleUrls: ['./person3.component.css']
})
export class Person3Component implements OnInit {

  constructor(private notify:NotifierService, private forms:FormBuilder) { }

  ChatForm = this.forms.group({
    input : [''],
    checkBox : [false]
  });

  get input(){
    return this.ChatForm.get('input');
  }
  get checkBox(){
    return this.ChatForm.get('checkBox');
  }
  currentMessage = {msg:null, checkBox:null, sender:null};
  ngOnInit(): void {
    this.notify.notify.subscribe((x)=>{
      this.currentMessage=x;
    })
  }

  sendMessage(){
    this.notify.sendNotify({msg:this.input?.value,checkBox:this.checkBox?.value,sender:'Person3'})
  }

}
