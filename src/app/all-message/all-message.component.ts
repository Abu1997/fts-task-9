import { Component, OnInit } from '@angular/core';
import { NotifierService } from '../notifier.service';

@Component({
  selector: 'app-all-message',
  templateUrl: './all-message.component.html',
  styleUrls: ['./all-message.component.css']
})
export class AllMessageComponent implements OnInit {
  
  constructor(private notify:NotifierService) { }

  currentMessage =  [{msg:'',checkBox:null,sender:''}];

  ngOnInit(): void {debugger
    this.notify.notify.subscribe(x => {this.currentMessage.push(x)} );
    console.log(this.currentMessage)
  }

}
